# JSIM Python Wrapper

A project to run simulations in JSIM and analyze the data in python directly. 

# Meta-setup (if you don't have a conda based package manager installed)
New York’s hottest club is… mamba! (Okay, you can do this with Anaconda Python, but I am migrating to use conda-forge and the associated free (in terms of licensing) community. Further, the development of a more efficient resolver motivates my recommendation to use the mambaforge installation. )

https://github.com/conda-forge/miniforge/#download

Otherwise most of the instructions reduce to launching a terminal with mamba/conda on the path and using it to create and activate the relevant environment. The environment file specifies that `conda-forge` should be used, if you manually install packaged you may need to use the `conda install -c conda-forge $PACKAGE` to ensure it is installed from `conda-forge`. 

# Setup
Once you have a mamba (or conda) executable you can create the environment as follows:
```
mamba env create -f environment.yml
mamba activate jsim
```
Now you're in a terminal with the right environment.
Just go change directory to where the code is and run
```
python setup.py install
```
To test the installation go to tests and run `test.py`:
```python
python test.py
```

## Linux
On Fedora 34 with JSim 18 everything is fine, however, to get JSim 2.19+ working we need to install `libgfortran.so.3`
This is available from conda forge so it should be  as easy as running (but this doesn't seem to get added to the path 
JSIM searches.)
```bash
mamba activate jsimpy #if not already active
mamba install libgfortran=3
```
(This way works!) If you perfer to install using the system tools see the following discussion:
https://ask.fedoraproject.org/t/fedora-30-install-gfortran-7/2856. Use this [`rpmfind`](https://rpmfind.net/linux/rpm2html/search.php?query=libgfortran.so.3%28%29%2864bit%29&submit=Search+...&system=&arch=) search and you can get an rpm for CentOS 8-stream that works.


## Windows
Once you’ve installed mambaforge you can use mamba (a drop in more efficient replacement for conda) to create an environment.
Typically will be necessary to find the shortcut to launch a Command Prompt with mamba added to the environment. Then proceed as with the Linux instructions.

## MacOS
No specific issues.