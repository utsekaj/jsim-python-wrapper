//
JSim v1.1

unit conversion off; //on;
import nsrunit;
unit mmHg_second_per_ml = mmHg*second/ml;
unit ml_per_second2 = ml/second^2;
unit mmHg_per_ml = mmHg/ml;
unit per_second2 = second^(-2);
unit mmHg_second2_per_ml = mmHg*second^2/ml;
unit ml_per_mmHg = ml/mmHg;
unit ml_per_mmHg_per_kg = ml/mmHg/kilogram;
unit ml_per_second = ml/second;
unit per_ml = ml^(-1);
unit Sec_per_Beat = second;
unit Beats = dimensionless;
unit Beats_per_min = Beats*min^(-1);

math MODEL{
  realDomain t second; 
  
  // Constants 
  real V_tot = 250 ml;
  real E_max mmHg_per_ml;
  E_max = 3.0;
  real E_min mmHg_per_ml;
  E_min = 0.3;
  real T second;
  T = 60.0/73.0;
  real Z_ao mmHg_second_per_ml;
  Z_ao = 0.1;
  real C_ao ml_per_mmHg;
  C_ao = 1.0;
  real R_sys mmHg_second_per_ml;
  R_sys = 1.0;
  real C_sv ml_per_mmHg;
  C_sv = 10.0;
  real R_mv mmHg_second_per_ml;
  R_mv = 0.006;
  real t_peak second;
  t_peak = 0.3; //0.28*T;
  real alpha dimensionless;
  alpha = 1.672;  
  real n1 dimensionless;
  n1 = 1.32;
  real n2 dimensionless;
  n2 = 21.9;
  real a dimensionless;
  a = 0.708;
  real a2Factor dimensionless;
  a2Factor = 1.677;

  real tau(t) second;
  real split(t) dimensionless;
	event( split>=1)
	{
	 split = 0;
	 tau = 0;
	}
	//tau = rem(t, T); //(1 second)*(Psi - 1*floor(Psi/1)); //rem(Psi,1); //rem doesn't translate to SBML
  
  
  //real alpha_inv = ((1.0/a)^n1)/(1.0+ (1.0/a)^n1)*(1.0/(1.0+ (1.0/(a*a2Factor))^n2));
  //real alpha = 1.0/alpha_inv;
  real a1 = a*t_peak/T;
  real a2 = a2Factor*a1;
  real shapeFunction1(t) = ((tau/(a1*T))^n1)/(1.0+(tau/(a1*T))^n1);
  real shapeFunction2(t) = (1.0+(tau/(a2*T))^n2);
  real e_t(t) dimensionless;
	e_t = alpha*shapeFunction1/shapeFunction2;
  real E(t) mmHg_per_ml;
  E = (E_max - E_min) * alpha*shapeFunction1/shapeFunction2 + E_min;
  

  // Define differential equations
  real P_ao(t) mmHg;
  real V_ao(t) = C_ao*P_ao;
  real P_sv(t) mmHg;
  real V_sv(t) = C_sv*P_sv;

  real V_lv(t) ml;

  // Compute flows
  real Q_lvao(t) ml_per_second;
  real Q_aosv(t) ml_per_second;
  real Q_svlv(t) ml_per_second;
  real P_lv(t) mmHg;

  P_lv =  E * V_lv;
  Q_lvao = if (P_lv > P_ao) (P_lv - P_ao)/Z_ao else 0;
  Q_aosv =  (P_ao-P_sv)/R_sys;
  Q_svlv = if (P_sv > P_lv) (P_sv - P_lv)/R_mv else 0;
  
  // initial conditions
  when (t = t.min){
		tau = 0;
		split = 0;
	  P_ao = 100;
	  V_lv = 100;
	  P_sv = (V_tot - V_lv -V_ao)/C_sv;
  }
  P_ao:t = (Q_lvao-Q_aosv)/C_ao;
  P_sv:t = (Q_aosv-Q_svlv)/C_sv;
  V_lv:t = Q_svlv-Q_lvao;
  split:t = 1/T;
  tau:t = 1;
}
