import os
import numpy as np
import matplotlib.pyplot as plt
# How to run jsim model on command line
# C:/Users/47948/Downloads/JSim_win64/win64/bin/jsbatch.bat -f C:\Users\47948\Desktop\Biomekanikk\model_wk.mod -oproj
# C:/Users/47948/Downloads/JSim_win64/win64/bin/jsbatch.bat -f C:\Users\47948\Desktop\Biomekanikk\model_wk.mod -ofmt column
import pandas as pd
import subprocess
import io
JSIM_INSTALL_DIR = 'C:/Users/jacobts/Documents/jsim_models/JSim_win64/win64/bin/'
path_to_model = "ve_linear_systemic.mod"

args = "E_max=3 E_min=0.2 P_th=-4 time.min=0.0 time.max=40.0 time.delta=0.010002500625156289 solver.ode_which=Auto".split(' ')

# Popen on Windows needs to specify individual arguments
mod_arglist = []
for arg in args:
    mod_arglist.append('-i')
    mod_arglist.append(arg)

JSIM_INSTALL_DIR = os.path.normpath(os.path.abspath('../jsimpy/JSim_win64/win64/bin'))
print(JSIM_INSTALL_DIR)
path_to_model = 've_linear_systemic.mod'
                  

arglist=[os.path.join(JSIM_INSTALL_DIR, "jsbatch.bat"), 
                  "-ofmt", "column"] + mod_arglist + ["-f", path_to_model,]
         
         
print(arglist)
argstr = ' '.join(arglist)
p = subprocess.Popen(arglist,
                  stdout=subprocess.PIPE,
                  stderr=subprocess.PIPE,
                  universal_newlines=True)
# p = subprocess.Popen(my_str,
                  # stdout=subprocess.PIPE,
                  # stderr=subprocess.PIPE,
                  # universal_newlines=True)
stdout, stderr = p.communicate()
print("stderr", stderr)
print("stdout", stdout)

# from jsbatch import jsbatch

# batch_args = ["-ofmt", "column"] + mod_arglist + ["-f", path_to_model,]

# JSIMHOME = os.path.normpath(os.path.abspath('../jsimpy/JSim_win64/'))
# stdout, stderr = jsbatch(batch_args, JSIMHOME)

# jsim_data = pd.read_csv(io.StringIO(stdout), sep="\t", skiprows=[1,])

# import jsim_simple_wrapper as jsw
# pars = dict(R=1.1)
# jsim_data = jsw.solve(path_to_model, 0, 5, 0.01, pars)
# print(jsim_data)
