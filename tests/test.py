import traceback
import numpy as np
try:
    import matplotlib.pyplot as plt
    interactive = True
except:
    plt = None
    interactive = False

import logging
import sys
root = logging.getLogger()
root.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root.addHandler(handler)

from jsimpy.wrapper import get_valid_pars_from_model, simulate, simulate_steady_state


def test_api(interactive=False):
    model_name = "ve_linear_systemic.mod"
    valid_pars = get_valid_pars_from_model(model_name)
    pars_dict = dict(E_max=3, E_min=0.3)
    external_inputs = dict(P_th="-0")

    T = 40.0
    max_step=0.01
    N = int(np.ceil(T/max_step))
    time_points = np.linspace(0,T, N)

    data = simulate(model_name, pars_dict, external_inputs, time_points, valid_pars=valid_pars)
    if interactive:
        plt.figure('api_test')
        plt.plot(data["var_dict"]["t"], data["var_dict"]["P_ao"])

    u0_ss, data = simulate_steady_state(model_name, pars_dict, external_inputs, time_points, valid_pars=valid_pars)
    if interactive:
        plt.figure('api_test')
        plt.plot(data["var_dict"]["t"], data["var_dict"]["P_ao"])
        plt.show()
    print('passed test_api')


def test_change_pars(interactive=False):
    model_name = "ve_linear_systemic.mod"
    valid_pars = get_valid_pars_from_model(model_name)
    pars_dict = dict(E_max=3, E_min=0.3)
    external_inputs = dict(P_th="-4")

    T = 40.0
    max_step=0.01
    N = int(np.ceil(T/max_step))
    time_points = np.linspace(0,T, N)

    u0_ss, data = simulate_steady_state(model_name, pars_dict, external_inputs, time_points, valid_pars=valid_pars)
    if interactive:
        plt.figure('change_pars')
        plt.plot(data["var_dict"]["t"], data["var_dict"]["P_ao"])

    pars_dict = dict(E_max=2.5, E_min=0.1)
    u0_ss, data = simulate_steady_state(model_name, pars_dict, external_inputs, time_points, valid_pars=valid_pars)
    if interactive:
        plt.figure('change_pars')
        plt.plot(data["var_dict"]["t"], data["var_dict"]["P_ao"])
        plt.show()
    print('passed test_change_pars')

def test_timevar_name(interactive=False):
    model_name="ve_linear_systemic_t.mod"
    valid_pars = get_valid_pars_from_model(model_name)
    pars_dict = dict(E_max=3, E_min=0.3)
    external_inputs = dict()

    T = 40.0
    max_step=0.01
    N = int(np.ceil(T/max_step))
    time_points = np.linspace(0,T, N)

    u0_ss, data = simulate_steady_state(model_name, pars_dict, external_inputs, time_points, valid_pars=valid_pars)
    if interactive:
        plt.figure()
        plt.plot(data["var_dict"]["t"], data["var_dict"]["P_ao"])
        plt.show()
    print('passed test_timevar_name')


def test_splitvar_name(interactive=False):
    model_name="ve_linear_systemic_split.mod"
    split_var = 'split'
    valid_pars = get_valid_pars_from_model(model_name)
    pars_dict = dict(E_max=3, E_min=0.3)
    external_inputs = dict()

    T = 40.0
    max_step=0.01
    N = int(np.ceil(T/max_step))
    time_points = np.linspace(0,T, N)

    u0_ss, data = simulate_steady_state(model_name, pars_dict, external_inputs, time_points, valid_pars=valid_pars,
                                        split_var=split_var)

    if interactive:
        plt.figure()
        plt.plot(data["var_dict"]["t"], data["var_dict"]["P_ao"])
        plt.show()
    print('passed test_splitvar_name')


def test_func_input(interactive=False):
    model_name="ve_linear_systemic.mod"
    valid_pars = get_valid_pars_from_model(model_name)
    pars_dict = dict(E_max=3, E_min=0.3)
    external_inputs = dict()
    external_inputs["P_th"] = '-4.0+if(time<20.0@or@time>30.0)0@else@40.0' # TODO add input check here

    T = 40.0
    max_step=0.01
    N = int(np.ceil(T/max_step))
    time_points = np.linspace(0, T, N)

    u0_ss, data = simulate_steady_state(model_name, pars_dict, external_inputs, time_points, valid_pars=valid_pars)

    if interactive:
        plt.figure()
        plt.plot(data["var_dict"]["t"], data["var_dict"]["P_ao"])
        plt.show()
    print('passed test_func_input')



if __name__ == "__main__":
    try:
        test_api(interactive=interactive)
    except Exception as e:
        print('Failed test_api')
        traceback.print_exc()
    try:
        test_change_pars(interactive=interactive)
    except Exception as e:
        print('Failed test_change_pars')
        traceback.print_exc()
    try:
        test_func_input(interactive=interactive)
    except Exception as e:
        print('Failed test_func_input')
        traceback.print_exc()
    try:
        test_timevar_name(interactive=interactive)
    except Exception as e:
        print('Failed test_timevar_name')
        traceback.print_exc()
    try:
        test_splitvar_name(interactive=interactive)
    except Exception as e:
        print('Failed test_splitvar_name')
        traceback.print_exc()
