# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='jsimpy',
    version='0.1.0',
    description='Python wrapper to run JSim models',
    long_description=readme,
    author='Jacob Sturdy',
    author_email='jacob.sturdy@gmail.com',
    url='https://gitlab.com/utsekaj/jsim-python-wrapper',
    license=license,
    packages=find_packages(exclude=('tests', 'docs')),
    include_package_data=True,
    install_requires=['numpy']

)

