#!/usr/bin/env python3
# JSIM reference on batch processing
# <http://www.physiome.org/jsim/docs/Devel_Batch.html#func>
import os
import platform
import time
import logging
import subprocess
import io
import xml.etree.ElementTree as ET

import numpy as np

cur = os.path.dirname(os.path.realpath(__file__))

logger = logging.getLogger(__name__)
JSBATCH_BUILD_SUFFIX = None
JSBATCH_SLEEP_DURATION = 2  # seconds
JSBATCH_MAX_RETRY = 5

LINUX_JSIM_INSTALL = "JSim_linux/linux/bin/"
WINDOWS_JSIM_INSTALL = "JSim_win64/win64/bin/"
MAC_JSIM_INSTALL = "JSim_macos/macos/bin/"

JSIM_INSTALL_DIR = {'Linux': LINUX_JSIM_INSTALL,
                    'Windows': WINDOWS_JSIM_INSTALL,
                    'Darwin': MAC_JSIM_INSTALL,
                    }
try:
    JSIM_INSTALL_DIR = JSIM_INSTALL_DIR[platform.system()]
except KeyError:
    raise NotImplementedError('JSim only supports Linux, Windows and Darwin (macOS). Your platform is %s.' % platform.system())

JSIMHOME = JSIM_INSTALL_DIR.split('/')[0]

JSBATCH = {'Linux':os.path.join(LINUX_JSIM_INSTALL, 'jsbatch'),
           'Windows':os.path.join(WINDOWS_JSIM_INSTALL, 'jsbatch.bat'),
           'Darwin':os.path.join(MAC_JSIM_INSTALL, 'jsbatch'),
           }[platform.system()]

JSIM_INSTALL_DIR = os.path.join(cur, JSIM_INSTALL_DIR)
JSIMHOME = os.path.join(cur, JSIMHOME)
JSBATCH = os.path.join(cur, JSBATCH)
os.environ['JSIMHOME'] = os.path.normpath(os.path.abspath(JSIMHOME))


def get_default_solver_options():
    """ Options extracted from jsim project xml file"""
    solver_options ={"ode_which" : "Auto",
                     "ode_Dopri5_reltol" : "1E-7",
                     "ode_Dopri5_abstol" : "1E-7",
                     "ode_Dopri5_nstep" : "100000",
                     "ode_Dopri5_stiff" : "1000",
                     "ode_Dopri5_round" : "2.3E-16",
                     "ode_Dopri5_safety" : ".9",
                     "ode_Dopri5_loselect" : ".2",
                     "ode_Dopri5_hiselect" : "10",
                     "ode_Dopri5_beta" : ".04",
                     "ode_Euler_nstep" : "2",
                     "ode_RK2_nstep" : "2",
                     "ode_Fehlberg_minstep" : "1E-4",
                     "ode_Fehlberg_maxstep" : ".1",
                     "ode_Fehlberg_tol" : "1E-6",
                     "ode_KM_minstep" : "1E-4",
                     "ode_KM_maxstep" : ".1",
                     "ode_KM_tol" : "1E-6",
                     "ode_Radau_reltol" : "1E-4",
                     "ode_Radau_abstol" : "1E-7",
                     "ode_Radau_nstep" : "100000",
                     "ode_Radau_round" : "1E-16",
                     "ode_Radau_safety" : ".9",
                     "ode_Radau_minord" : "3",
                     "ode_Radau_maxord" : "7",
                     "ode_Radau_initord" : "3",
                     "ode_Radau_newton" : "7",
                     "ode_Radau_jacob" : ".001",
                     "ode_Radau_losize" : "1",
                     "ode_Radau_hisize" : "1.2",
                     "ode_Radau_loselect" : ".2",
                     "ode_Radau_hiselect" : "8",
                     "ode_Radau_locontract" : ".002",
                     "ode_Radau_hicontract" : ".8",
                     "ode_Radau_hiorder" : "1.2",
                     "ode_Radau_loorder" : ".8",
                     "ode_RK4_nstep" : "2",
                     "ode_CVode_reltol" : "1E-7",
                     "ode_CVode_abstol" : "1E-8",
                     "ode_CVode_maxsteps" : "100000",
                     "ode_CVode_stiff" : "false"}
    return solver_options


def get_valid_parameters(tree):
    """
    Generate dictionary of the project Top level is "models" which is dict where the keys are model names
    each models["model_name"] is also a dictionary containing three dictionaries corresponding to the 
    "pargroups" of the JSIM project namely inputs, solver and memory
    Inputs:
        tree (xml.etree.ElementTree.ElementTree): the xml tree
        root (xml.etree.ElementTree.Element): the root of the xml tree
    """
    project = get_project_parameters(tree)
    names = set(next(iter(project["models"].values()))["inputs"].keys())
    values = set(next(iter(project["models"].values()))["inputs"].values())
    return names


def get_project_parameters(tree):
    """
    Generate dictionary of the project Top level is "models" which is dict where the keys are model names
    each models["model_name"] is also a dictionary containing three dictionaries corresponding to the 
    "pargroups" of the JSIM project namely inputs, solver and memory
    Inputs:
        tree (xml.etree.ElementTree.ElementTree): the xml tree
        root (xml.etree.ElementTree.Element): the root of the xml tree
    """
    root = tree.getroot()
    project = {"models":{}}
    for model in root.findall('project/model'):
        model_data = {}
        for pargroup in model.findall('parset/pargroups/pargroup'): # TODO: there may be multiple parsets (the default one is "last")
            pargroup_data = {}
            for par in pargroup:
                pargroup_data[par.get("name")] = par.get("value")
            model_data[pargroup.get("name")] = pargroup_data
        project["models"][model.get("name")] = model_data
    return project


def update_project_parameters(tree, project):
    """
    Inputs:
        tree (xml.etree.ElementTree.ElementTree): the xml tree
        root (xml.etree.ElementTree.Element): the root of the xml tree
    """
    root = tree.getroot()
    for model in root.findall('project/model'):
        if model.get("name") in project["models"]:
            model_dict = project["models"][model.get("name")]
            for pargroup in model.findall('parset/pargroups/pargroup'): # TODO: there may be multiple parsets (the default one is "last")
                pargroup_name = pargroup.get("name")
                if pargroup_name in model_dict:
                    pars_dict = model_dict[pargroup.get("name")]
                    for par in pargroup:
                        par_name = par.get("name")
                        if par_name in pars_dict:
                            par.set("value", str(pars_dict[par_name])) # TODO: where should validation that it's a string/well formed be?
    return tree


def get_valid_pars_from_model(model):
    """ generate set of all valid parameter names"""
    path_to_model = model

    path_to_model=os.path.abspath(path_to_model)
    builddirsfx = str(os.getpid())
    for i in range(0, JSBATCH_MAX_RETRY):
        p = subprocess.Popen([JSBATCH,
                              "-f", path_to_model, "-oproj"],
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                universal_newlines=True)
        stdout, stderr = p.communicate()
        filesystem_failure = ("file or directory" in stderr or 
                              "Failed to create build directory" in stderr)
        if filesystem_failure:
            logger.info("JSIM JSBATCH fs failure in get_valid_pars_from_model")
            builddirsfx = str(i) + builddirsfx
            logger.debug("No data for xml on try #{}".format(i+1))
            logger.debug("stdout from JSIM: {}".format(stdout))
            logger.debug("stderr from JSIM: {}".format(stderr))
            logger.debug("Try #{} failed. Sleep {} seconds and try again.".format(i+1, JSBATCH_SLEEP_DURATION))
            time.sleep(JSBATCH_SLEEP_DURATION)
            continue
        break
    if filesystem_failure:
        logger.exception("JSIM failed to parse model. Raising exception.")
        raise FileNotFoundError("JSIM couldn't read filesystem", builddirsfx, os.getpid(), stderr)
    project_xml = io.StringIO(stdout)
    tree = ET.parse(project_xml)
    valid_pars = get_valid_parameters(tree)
    return valid_pars


def solve(path_to_model, tmin, tmax, max_step, pars, external_inputs, valid_pars=None, atol=1e-8,
        rtol=1e-7, solver="Auto"):
    if valid_pars is None:
        valid_pars = get_valid_pars_from_model(path_to_model)
    pars_strings = []

    for key, value in pars.items():  # This is the fastest as compared to filters and or comprehensions
        if key in valid_pars:
            pars_strings.append('{}={}'.format(key, value))
    jsim_pars_string = ' '.join(pars_strings)
    
    ext_strings = ''
    for key, value in external_inputs.items():
        if key in valid_pars:
            ext_strings += '{}={}'.format(key, value)

    # TODO t vs time as main domain... should be able to parse .mod to determine
    if 'time.min' in valid_pars:
        domain = 'time'
    elif 't.min' in valid_pars:
        domain = 't'
    else:
        raise NotImplementedError('The model %s has no time domain named "t" or "time"', path_to_model)
    jsim_solver_string= f'{domain}.min={tmin} {domain}.max={tmax} {domain}.delta={max_step}'
    solver_options = get_default_solver_options()
    solver_options["ode_CVode_stiff"] = "true"
    solver_options["ode_CVode_reltol"] = atol
    solver_options["ode_CVode_abstol"] = rtol
    solver_options["ode_Dopri5_reltol"] = atol
    solver_options["ode_Dopri5_abstol"] = rtol

    solver_options["ode_Fehlberg_tol"] = atol
    solver_options["ode_Fehlberg_maxstep"] = max_step

    solver_options["ode_Radau_reltol"] = atol
    solver_options["ode_Radau_abstol"] = rtol


    builddirsfx = str(os.getpid())
    terminate_loop = False
    loop_count = 0
    solvers = ["Auto", "CVode", "Radau"]
    solver = solvers.pop(0)
    while not terminate_loop:
        loop_count += 1
        terminate_loop = loop_count >= JSBATCH_MAX_RETRY
                
        logger.debug("Solving in JSIM with solver = {}.".format(solver))
        solver_options["ode_which"] = solver
        solver_options_string = ' '.join(['solver.{}={}'.format(key, value) for key, value
                                in solver_options.items()])

        # TODO: better way to efficiently iterate through these? Don't make strings in the first place?
        if ext_strings == '':
            args_list =' '.join([jsim_pars_string, jsim_solver_string, solver_options_string]).split(' ')
        else:
            args_list = ' '.join([jsim_pars_string, ext_strings, jsim_solver_string, solver_options_string]).split(' ')
        inputs_list = 2*len(args_list)*[None,]
        for idx, input in enumerate(args_list):
            inputs_list[2*idx] = '-i'
            inputs_list[2*idx + 1] = input

        # TODO: This works for Linux, is it more efficient?
        # inputs_list = "-i " + " -i ".join([jsim_pars_string,
        #                                    ext_strings,
        #                                   jsim_solver_string,
        #                                   solver_options_string])
        # inputs_list = inputs_list.split(" ")
        
        p = subprocess.Popen([JSBATCH, "-ofmt", "column" ] +
                          inputs_list +
                          ["-f", path_to_model],
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          universal_newlines=True)  # FileNotFoundError -> no jsbatch on path -> raise
        stdout, stderr = p.communicate()
        solver_failure = "Solver Error" in stderr
        filesystem_failure = "file or directory" in stderr
        if solver_failure:
            logger.debug("Solver error in JSIM. solver = {}".format(solver))
            logger.exception("stderr from JSIM: {}".format(stderr))
            solver = solvers.pop(0) # Raises index error when out of solvers
        elif filesystem_failure:
            if terminate_loop:
                logger.exception("JSIM couldn't read filesystem on try #{}.".format(loop_count))
                logger.exception("stderr from JSIM: {}".format(stderr))
                logger.exception("Filesystem failure on try #{}. Raising exception.".format(loop_count))
                raise FileNotFoundError("JSIM couldn't read filesystem", builddirsfx, os.getpid(),
                        stderr)
            else:
                solvers = ["Auto", "CVode", "Radau"]
                solver = solvers.pop(0)
                builddirsfx = str(loop_count) + builddirsfx
                logger.debug("Try #{} failed. Sleep {} seconds and try again.".format(loop_count,
                                                                        JSBATCH_SLEEP_DURATION))
                time.sleep(JSBATCH_SLEEP_DURATION)
        else:
            terminate_loop = True
            break

    stream =  io.StringIO(stdout)
    delimiter='\t'
    headers = stream.readline().split(delimiter)
    if headers[-1] in ['\n', '\r\n']: #TODO better way to check for empty column:
        headers = headers[0:-1]

    raw_data = np.genfromtxt(stream, delimiter=delimiter, skip_header=1)
    jsim_data = dict()
    for idx, column in enumerate(headers):
        column = column.replace('"', '')
        jsim_data[column] = raw_data[:, idx]

    if domain == "t":
        jsim_data['time'] = jsim_data.pop(domain)

    return jsim_data


def split(jsim_dict, split_var="psi", split_val=1.0):
    """
    Split the solution dataframe into separate dictionaries for each cycle identified according to the split_var
    Inputs:
        jsim_dict (dict): results of a jsim_simulation
        split_var (str): name of variable to split data
        split_val (float): value of split_var at which a new cycle is defined to start
    Returns:
        jsim_cycle_dict (dict): dictionary containing the data from the last full cycle of the simulation
        jsim_dict (dict): dictionary containing the full solution data as well as 'events', the indices points when a
        new cycle starts
    """
    time = jsim_dict["time"]
    tau_vals = jsim_dict[split_var]
    zero_crossings = np.where(np.diff(np.sign(np.mod(tau_vals, 1) - split_val / 2)) < 0)[0]
    jsim_cycle_dict = {}
    if len(zero_crossings) > 1:
        last_period = time[zero_crossings[-1]] - time[zero_crossings[-2]]
        for key, values in jsim_dict.items():
            jsim_cycle_dict[key] = values[zero_crossings[-2]:zero_crossings[-1]]
            #jsim_cycle_data = jsim_data[zero_crossings[-2]:zero_crossings[-1]]
    else:
        jsim_cycle_data = None  # Not enough periods


    jsim_dict["events"] = zero_crossings
    jsim_dict["t"] = jsim_dict["time"]
    jsim_cycle_dict["t"] = jsim_cycle_dict["time"]
    return jsim_cycle_dict, jsim_dict


def cycle_analyzer(heart_beats, var_dict):
    """
    Analyzes the full heart beat properties of hemodynamic variables

    Takes a cardiovascular model assumed to have pulsatile behavior and state
    variable "Psi" which goes from 0 to 1 over the heart beat:
        * Calculates the actual heart rate and interbeat interval based on Psi
        * Calculates the stroke volume of left and right ventricles
        * Calculates cycle averages of all variables in var_dict
        * Calculates cycle systolic (max) value of all variables in var_dict
        * Calculates cycle diastolic (min) value of all variables in var_dict

    Returns:
        heart_interval (array): an array of the same shape as var_dict["t"] with the amount of time between the beginning and end of the current heart beat as values.
        heart_rates (array): 1/heart_interval
        means (dict): consisting a arrays of the same dimension as var_dict["t"] for each variable in var_dict with the mean value of the corresponding heart beat for all indices corresponding to a given heart beat.
        systolic (dict): same a means, but maximum values instead of mean
        diastolic (dict): same a means, but minimum values instead of mean
    """
    t = np.array(var_dict["t"])

    if len(heart_beats) == 1:
        cycles = [t >= t[0], ]
        inter_beat_intervals = np.array([t[-1] - t[0], ])
    else:
        cycles = [t >= t[0], ] + [t >= beat for beat in heart_beats]
        cycles = [np.logical_and(cycle, t < beat) for beat, cycle in zip(heart_beats, cycles[0:-1])] + [cycles[-1], ]
        inter_beat_intervals = np.diff(heart_beats)

    # Allocate arrays for values
    heart_rate = np.zeros_like(t)
    heart_intervals = np.zeros_like(t)
    stroke_volume = np.zeros_like(t)
    means = {}
    systolic = {}
    diastolic = {}
    # means = {var:np.zeros_like(t) for var in var_dict} # Which way is faster?
    for var in var_dict:
        if np.shape(var_dict[var]) == t.shape:
            means[var] = np.zeros_like(t)
            systolic[var] = np.zeros_like(t)
            diastolic[var] = np.zeros_like(t)

    for cycle, heart_interval in zip(cycles, inter_beat_intervals):
        heart_intervals[cycle] = heart_interval
        heart_rate[cycle] = 60.0 / heart_interval
        t_cycle = t[cycle]
        stroke_volume[cycle] = np.max(var_dict["V_lv"][cycle]) - np.min(var_dict["V_lv"][cycle])

        for var in means.keys():
            values = var_dict[var][cycle]
            means[var][cycle] = np.trapz(values, t_cycle) / (t_cycle[-1] - t_cycle[0])
            systolic[var][cycle] = np.max(values)
            diastolic[var][cycle] = np.min(values)

    return heart_intervals, heart_rate, stroke_volume, means, systolic, diastolic


def simulate(model, pars, external_inputs, time_points, valid_pars=None, atol=1e-10, rtol=1e-9, solver='Auto', split_var="tau"):
    """
    Simulate model for given conditions
    Args:
        model (filepath): the name of
        pars (dict): a dictionary of parameter values for the model
        external_inputs (dict): a dictionary of external inputs
        time_points (array): array of time points defining range of values simulated over. max step is inferred from the
                            spacing
        valid_pars (dict): the entries in pars and external inputs are only inserted into the model if the corresponding
                            key is found in valid_pars. When valid_pars=None (default), it will be generated from the
                            model directly.
        atol(float): absolute tolerance for solvers
        rtol(float): relative tolerance for solvers
        solver(str): which solver to use 'Auto', 'Cvode', 'Radau', 'Dopri5'. See JSIM for documentation
        split_var(str): What variable to use to split the solution data into cycles.
    Returns:
       simulation_data (dict):
    """
    assert isinstance(model, str), "model is not a string. It is %s." % model
    model_name = model

    jsim_data = solve(model_name, time_points[0], time_points[-1], time_points[1] - time_points[0],
                      pars, external_inputs, valid_pars=valid_pars, atol=atol, rtol=rtol, solver=solver)

    var_dict, jsim_data = split(jsim_data, split_var=split_var)

    heart_beats = jsim_data["t"][jsim_data["events"]]

    heart_intervals, heart_rate, stroke_volume, means, systolic, diastolic = cycle_analyzer(heart_beats, jsim_data)
    simulation_data = dict(t=jsim_data["t"],
                           pars=pars,
                           var_dict=jsim_data,
                           events=jsim_data["events"],
                           external_inputs=external_inputs,
                           sundials_error=None)

    simulation_data["cycle_data"] = dict(heart_intervals=heart_intervals, heart_rate=heart_rate,
                                         stroke_volume=stroke_volume, means=means, systolic=systolic,
                                         diastolic=diastolic)

    return simulation_data


def simulate_steady_state(model, pars, external_inputs, time_points, valid_pars=None, atol=1e-10,
                          rtol=1e-9, solver='Auto', split_var="tau"):
    """
    Attempts to find a steady state of model

    Args:
        model (str or object): the name of  the model
        pars (dict): a dictionary of parameter values for the model
        external_inputs (dict): a dictionary of external inputs
        time_points (array): array of time points defining range of values simulated over. max step is inferred from the
                            spacing
        valid_pars (dict): the entries in pars and external inputs are only inserted into the model if the corresponding
                            key is found in valid_pars. When valid_pars=None (default), it will be generated from the
                            model directly.
        atol(float): absolute tolerance for solvers
        rtol(float): relative tolerance for solvers
        solver(str): which solver to use 'Auto', 'Cvode', 'Radau', 'Dopri5'. See JSIM for documentation
        split_var(str): What variable to use to split the solution data into cycles.

    Returns:
       u0_ss (None): place holder value always None
       simulation_data (dict):

    """
    assert isinstance(model, str), "model is not a string. It is %s." % model
    model_name = model

    jsim_data = solve(model_name, time_points[0], time_points[-1],
                      time_points[1] - time_points[0], pars,
                      external_inputs,
                      valid_pars=valid_pars,
                      atol=atol,
                      rtol=rtol,
                      solver=solver)

    var_dict, jsim_data = split(jsim_data, split_var=split_var)

    heart_beats = [var_dict["t"][0], ]

    heart_intervals, heart_rate, stroke_volume, means, systolic, diastolic = cycle_analyzer(heart_beats, var_dict)
    # TODO: return full simulation also?
    simulation_data = dict(t=var_dict["t"],
                           pars=pars,
                           var_dict=var_dict,
                           external_inputs=external_inputs,
                           sundials_error=None)

    simulation_data["cycle_data"] = dict(heart_intervals=heart_intervals, heart_rate=heart_rate,
                                         stroke_volume=stroke_volume,
                                         means=means,
                                         systolic=systolic,
                                         diastolic=diastolic)

    return None, simulation_data

